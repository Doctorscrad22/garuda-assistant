<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>GarudaAssistant</name>
    <message>
        <location filename="../garudaassistant.ui" line="14"/>
        <source>Garuda-Assistant</source>
        <translation type="unfinished">Garuda Assistent</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="31"/>
        <source>Maintenance</source>
        <translation type="unfinished">Wartung</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="37"/>
        <source>Reset Configs</source>
        <translation type="unfinished">Konfiguration zurücksetzen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="46"/>
        <source>Reset Selected Configs (This might overwrite your customizations)</source>
        <translation type="unfinished">Ausgewählte Konfigurationen zurücksetzen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="71"/>
        <source>System</source>
        <translation type="unfinished">System</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="137"/>
        <source>Clear package cache</source>
        <translation type="unfinished">Zwischengespeicherte Pakete löschen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="157"/>
        <source>System update</source>
        <translation type="unfinished">System aktualisieren</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="117"/>
        <source>Edit repositories</source>
        <translation type="unfinished">Repositories bearbeiten</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="197"/>
        <source>Remove orphans</source>
        <translation type="unfinished">Verwaiste Pakete entfernen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="177"/>
        <source>Clear caches</source>
        <translation type="unfinished">Cache löschen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="237"/>
        <source>Refresh mirrorlist (Reflector-Simple)</source>
        <translation type="unfinished">Spiegelserverliste erneuern</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="77"/>
        <source>Remove database lock</source>
        <translation type="unfinished">Pacman Datenbank entsperren</translation>
    </message>
    <message>
        <source>Refresh Keyrings</source>
        <translation type="obsolete">Keyrings erneuern</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="217"/>
        <source>Reinstall all packages</source>
        <translation type="unfinished">Alle Pakete neu installieren</translation>
    </message>
    <message>
        <source>Btrfs</source>
        <translation type="obsolete">BTRFS</translation>
    </message>
    <message>
        <source>Internal Filesystem Statistics</source>
        <translation type="obsolete">Dateisystem Statistiken</translation>
    </message>
    <message>
        <source>Data:</source>
        <translation type="obsolete">Daten:</translation>
    </message>
    <message>
        <source>Metadata:</source>
        <translation type="obsolete">Metadaten:</translation>
    </message>
    <message>
        <source>Balance not currently needed</source>
        <translation type="obsolete">Balancing nicht erforderlich</translation>
    </message>
    <message>
        <source>Start Full Balance</source>
        <translation type="obsolete">Balancing starten</translation>
    </message>
    <message>
        <source>System:</source>
        <translation type="obsolete">System:</translation>
    </message>
    <message>
        <source>Information</source>
        <translation type="obsolete">Informationen</translation>
    </message>
    <message>
        <source>Used:</source>
        <translation type="obsolete">Benutzt:</translation>
    </message>
    <message>
        <source>Allocated:  </source>
        <translation type="obsolete">Zugewiesen:  </translation>
    </message>
    <message>
        <source>Filesystem Size: </source>
        <translation type="obsolete">Dateisystemgröße: </translation>
    </message>
    <message>
        <source>Free(Estimated): </source>
        <translation type="obsolete">Frei (geschätzt): </translation>
    </message>
    <message>
        <source>Device Selection</source>
        <translation type="obsolete">Gerät auswählen</translation>
    </message>
    <message>
        <source>Device:</source>
        <translation type="obsolete">Gerät:</translation>
    </message>
    <message>
        <source>Show subvolume tab</source>
        <translation type="obsolete">Subvolume Tab anzeigen</translation>
    </message>
    <message>
        <source>none</source>
        <translation type="obsolete">keine</translation>
    </message>
    <message>
        <source>Refresh BTRFS Data</source>
        <translation type="obsolete">BTRFS Daten laden</translation>
    </message>
    <message>
        <source>Timers</source>
        <translation type="obsolete">Timer</translation>
    </message>
    <message>
        <source>BTRFS scrub enabled</source>
        <translation type="obsolete">BTRFS scrub an</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="594"/>
        <location filename="../garudaassistant.ui" line="677"/>
        <source>service</source>
        <translation type="unfinished">service</translation>
    </message>
    <message>
        <source>btrfs-scrub.timer</source>
        <translation type="obsolete">btrfs-scrub.timer</translation>
    </message>
    <message>
        <source>BTRFS balance enabled</source>
        <translation type="obsolete">BTRFS Balancing an</translation>
    </message>
    <message>
        <source>btrfs-balance.timer</source>
        <translation type="obsolete">btrfs-balance.timer</translation>
    </message>
    <message>
        <source>BTRFS trim enabled</source>
        <translation type="obsolete">BTRFS Trim an</translation>
    </message>
    <message>
        <source>btrfs-trim.timer</source>
        <translation type="obsolete">btrfs-trim.timer</translation>
    </message>
    <message>
        <source>BTRFS defrag enabled</source>
        <translation type="obsolete">BTRFS defragmentieren ein</translation>
    </message>
    <message>
        <source>btrfs-defrag.timer</source>
        <translation type="obsolete">btrfs-defrag.timer</translation>
    </message>
    <message>
        <source>Snapper timeline enabled</source>
        <translation type="obsolete">Snapper Timeline an</translation>
    </message>
    <message>
        <source>snapper-timeline.timer</source>
        <translation type="obsolete">snapper-timerline.timer</translation>
    </message>
    <message>
        <source>Snapper cleanup enabled</source>
        <translation type="obsolete">Snapper aufräumen ein</translation>
    </message>
    <message>
        <source>snapper-cleanup.timer</source>
        <translation type="obsolete">snapper-cleanup.timer</translation>
    </message>
    <message>
        <source>Snapper boot enabled</source>
        <translation type="obsolete">Snapper booten an</translation>
    </message>
    <message>
        <source>snapper-boot.timer</source>
        <translation type="obsolete">snapper-boot.timer</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1138"/>
        <location filename="../garudaassistant.ui" line="1614"/>
        <source>Apply</source>
        <translation type="unfinished">Anwenden</translation>
    </message>
    <message>
        <source>Btrfs Subvolumes</source>
        <translation type="obsolete">BTRFS Subvolumes</translation>
    </message>
    <message>
        <source>Include Timeshift and Snapper Snapshots</source>
        <translation type="obsolete">Timeshift und Snapper Snapshots anzeigen</translation>
    </message>
    <message>
        <source>Refresh Subvolumes</source>
        <translation type="obsolete">Subvolumes neu laden</translation>
    </message>
    <message>
        <source>Restore Snapshot</source>
        <translation type="obsolete">Snapshot wiederherstellen</translation>
    </message>
    <message>
        <source>Delete Selected</source>
        <translation type="obsolete">Ausgewählte löschen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="97"/>
        <source>Refresh keyrings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Snapper</source>
        <translation type="vanished">Snapper</translation>
    </message>
    <message>
        <source>Select config: </source>
        <translation type="obsolete">Konfiguration auswählen: </translation>
    </message>
    <message>
        <source>New Snapshot</source>
        <translation type="obsolete">Neuer Snapshot</translation>
    </message>
    <message>
        <source>Delete Snapshot</source>
        <translation type="obsolete">Snapshot löschen</translation>
    </message>
    <message>
        <source>Show settings tab</source>
        <translation type="obsolete">Einstellungen anzeigen</translation>
    </message>
    <message>
        <source>Snapper - Settings</source>
        <translation type="obsolete">Snapper - Einstellungen</translation>
    </message>
    <message>
        <source>New Config</source>
        <translation type="obsolete">Neue Konfiguration</translation>
    </message>
    <message>
        <source>Delete Config</source>
        <translation type="obsolete">Konfiguration löschen</translation>
    </message>
    <message>
        <source>Save Config</source>
        <translation type="obsolete">Konfiguration speichern</translation>
    </message>
    <message>
        <source>Config information</source>
        <translation type="obsolete">Konfiguration - Informationen</translation>
    </message>
    <message>
        <source>Config name: </source>
        <translation type="obsolete">Name: </translation>
    </message>
    <message>
        <source>Backup path: </source>
        <translation type="obsolete">Backup Pfad: </translation>
    </message>
    <message>
        <source>Snapshot retention</source>
        <translation type="obsolete">Snapshots behalten</translation>
    </message>
    <message>
        <source>Save: </source>
        <translation type="obsolete">Speichern: </translation>
    </message>
    <message>
        <source>Hourly</source>
        <translation type="obsolete">Stündlich</translation>
    </message>
    <message>
        <source>Daily</source>
        <translation type="obsolete">Täglich</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1863"/>
        <source>Last pacman log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekly</source>
        <translation type="obsolete">Wöchentlich</translation>
    </message>
    <message>
        <source>Monthly</source>
        <translation type="obsolete">Monatlich</translation>
    </message>
    <message>
        <source>Yearly</source>
        <translation type="obsolete">Jährlich</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="403"/>
        <source>System Components</source>
        <translation type="unfinished">Systemeinstellungen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="990"/>
        <source>Input method</source>
        <translation type="unfinished">Eingabemethode</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="996"/>
        <source>Fcitx</source>
        <translation type="unfinished">Fcitx</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="664"/>
        <location filename="../garudaassistant.ui" line="690"/>
        <location filename="../garudaassistant.ui" line="999"/>
        <location filename="../garudaassistant.ui" line="1012"/>
        <location filename="../garudaassistant.ui" line="1025"/>
        <location filename="../garudaassistant.ui" line="1038"/>
        <source>package</source>
        <translation type="unfinished">Packet</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="935"/>
        <source>PulseAudio support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="961"/>
        <source>PipeWire support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1002"/>
        <source>fcitx-input-support</source>
        <translation type="unfinished">fcitx-input-support</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1009"/>
        <source>Ibus</source>
        <translation type="unfinished">Ibus</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1015"/>
        <source>ibus-input-support</source>
        <translation type="unfinished">ibus-input-support</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1022"/>
        <source>Fcitx5</source>
        <translation type="unfinished">Fcitx5</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1028"/>
        <source>fcitx5-input-support</source>
        <translation type="unfinished">fcitx5-input-support</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1035"/>
        <source>Asian fonts</source>
        <translation type="unfinished">Asiatische Fonts</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1041"/>
        <source>asian-fonts</source>
        <translation type="unfinished">asian-fonts</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="485"/>
        <source>Connman</source>
        <translation type="unfinished">Connman</translation>
    </message>
    <message>
        <source>connman.service</source>
        <translation type="obsolete">connman.service</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="504"/>
        <source>Connman support</source>
        <translation type="unfinished">Connman Unterstützung</translation>
    </message>
    <message>
        <source>connman-support</source>
        <translation type="obsolete">Connman Unterstützung</translation>
    </message>
    <message>
        <source>ofono.service</source>
        <translation type="obsolete">ofono.service</translation>
    </message>
    <message>
        <source>neard.service</source>
        <translation type="obsolete">neard.service</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="424"/>
        <source>Samba</source>
        <translation type="unfinished">Samba</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="430"/>
        <source>User in sambashare group</source>
        <translation type="unfinished">Benutzer in sambashare Gruppe</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="651"/>
        <location filename="../garudaassistant.ui" line="703"/>
        <location filename="../garudaassistant.ui" line="716"/>
        <source>group</source>
        <translation type="unfinished">Gruppe</translation>
    </message>
    <message>
        <source>sambashare</source>
        <translation type="obsolete">sambashare</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="443"/>
        <source>SMB enabled</source>
        <translation type="unfinished">Samba benutzen</translation>
    </message>
    <message>
        <source>smb.service</source>
        <translation type="obsolete">smb.service</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="456"/>
        <source>Samba support</source>
        <translation type="unfinished">Samba benutzen</translation>
    </message>
    <message>
        <source>samba-support</source>
        <translation type="obsolete">Samba Unterstützung</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="469"/>
        <source>NMB service</source>
        <translation type="unfinished">NMB Service</translation>
    </message>
    <message>
        <source>nmb.service</source>
        <translation type="obsolete">nmb.service</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1051"/>
        <source>Firewall</source>
        <translation type="unfinished">Firewall</translation>
    </message>
    <message>
        <source>ufw enabled</source>
        <translation type="obsolete">UFW benutzen</translation>
    </message>
    <message>
        <source>ufw</source>
        <translation type="obsolete">UFW</translation>
    </message>
    <message>
        <source>firewalld</source>
        <translation type="obsolete">Firewalld</translation>
    </message>
    <message>
        <source>firewalld enabled</source>
        <translation type="obsolete">Firewalld benutzen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="546"/>
        <source>NetworkManager</source>
        <translation type="unfinished">Netzwerkmanager</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="552"/>
        <source>NetworkManager support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="565"/>
        <source>NetworkManager enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="578"/>
        <source>ModemManager enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="591"/>
        <source>gpsd enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="597"/>
        <source>gpsd.service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="842"/>
        <source>Bluetooth</source>
        <translation type="unfinished">Bluetooth</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="848"/>
        <source>Bluetooth support</source>
        <translation type="unfinished">Bluetooth Unterstützung</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="861"/>
        <source>User in lp group</source>
        <translation type="unfinished">Benutzer in lp Gruppe</translation>
    </message>
    <message>
        <source>lp</source>
        <translation type="obsolete">lp</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="874"/>
        <source>Bluetooth enabled</source>
        <translation type="unfinished">Bluetooth aktiviert</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="887"/>
        <source>Pulseaudio-bluetooth-autoconnect enabled</source>
        <translation type="unfinished">PA Bluetooth automatisch verbinden</translation>
    </message>
    <message>
        <source>global_service</source>
        <translation type="obsolete">Globaler Service</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="900"/>
        <source>Bluetooth-autoconnect enabled</source>
        <translation type="unfinished">Bluetooth automatisch verbinden</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="607"/>
        <source>Video</source>
        <translation type="unfinished">Video</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="613"/>
        <source>Gstreamer Codecs</source>
        <translation type="unfinished">GStreamer Codecs</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="626"/>
        <source>User in video group</source>
        <translation type="unfinished">Benutzer in video Gruppe</translation>
    </message>
    <message>
        <source>video</source>
        <translation type="obsolete">video</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="642"/>
        <source>Virtualization</source>
        <translation type="unfinished">Virtualisierung</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="648"/>
        <source>User in vboxusers group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="654"/>
        <source>vboxusers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="661"/>
        <source>Virtualbox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="667"/>
        <source>virtualbox-meta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="674"/>
        <source>libvirtd enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="680"/>
        <source>libvirtd.service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="687"/>
        <source>Virt-manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="693"/>
        <source>virt-manager-meta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="700"/>
        <source>User in libvirt group</source>
        <translation type="unfinished">Benutzer in libvirt Gruppe</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="706"/>
        <source>libvirt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="713"/>
        <source>User in kvm group</source>
        <translation type="unfinished">Benutzer in kvm Gruppe</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="719"/>
        <source>kvm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="729"/>
        <source>Printing and Scanning</source>
        <translation type="unfinished">Drucken und Scannen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="735"/>
        <source>User in cups group</source>
        <translation type="unfinished">Benutzer in cups Gruppe</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1575"/>
        <source>Profile-sync-daemon enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1588"/>
        <source>Systemd-oomd enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>cups</source>
        <translation type="obsolete">cups</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="748"/>
        <source>CUPS enabled</source>
        <translation type="unfinished">CUPS aktiviert</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="761"/>
        <source>Saned enabled</source>
        <translation type="unfinished">Saned eingeschaltet</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="491"/>
        <source>Connman enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="517"/>
        <source>oFono enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="530"/>
        <source>Neard enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="774"/>
        <source>IPP USB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="787"/>
        <source>Printing support</source>
        <translation type="unfinished">Druckeruntersützung</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="800"/>
        <source>Scanning support</source>
        <translation type="unfinished">Scannerunterstützung</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="813"/>
        <source>User in sys group</source>
        <translation type="unfinished">Benutzer in sys Gruppe</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="826"/>
        <source>User in scanner group</source>
        <translation type="unfinished">Benutzer in scanner Gruppe</translation>
    </message>
    <message>
        <source>scanner</source>
        <translation type="obsolete">Scanner</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="916"/>
        <source>Audio</source>
        <translation type="unfinished">Audio</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="922"/>
        <source>JACK support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pulseaudo support</source>
        <translation type="obsolete">PulseAudio Unterstützung</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="948"/>
        <source>ALSA support</source>
        <translation type="unfinished">ALSA Unterstützung</translation>
    </message>
    <message>
        <source>Pipewire support</source>
        <translation type="obsolete">PipeWire Unterstützung</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="974"/>
        <source>User in realtime group</source>
        <translation type="unfinished">Benutzer in realtime Gruppe</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1146"/>
        <source>Settings</source>
        <translation type="unfinished">Einstellungen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1484"/>
        <source>Guest user</source>
        <translation type="unfinished">Gastnutzer</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1490"/>
        <source>Guest user support</source>
        <translation type="unfinished">Gast in user Gruppe</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1327"/>
        <source>Performance Tweaks</source>
        <translation type="unfinished">Performance Tweaks</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1333"/>
        <source>Performance tweaks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1346"/>
        <source>Ananicy Cpp enabled</source>
        <translation type="unfinished">Ananicy Cpp aktiviert</translation>
    </message>
    <message>
        <source>UResourced enabled</source>
        <translation type="obsolete">UResourced aktiviert</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1359"/>
        <source>Irqbalance enabled</source>
        <translation type="unfinished">Irqbalance aktiviert</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1531"/>
        <source>Right click emulation</source>
        <translation type="unfinished">Rechtsklick Emulierung</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1537"/>
        <source>Evdev long press right click emulation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1550"/>
        <source>Evdev-rce enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hblock</source>
        <translation type="obsolete">HBlock</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1512"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;An adblocker that creates a hosts file from automatically downloaded sources&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1515"/>
        <source>Hblock enabled</source>
        <translation type="unfinished">Hblock aktiviert</translation>
    </message>
    <message>
        <source>custom</source>
        <translation type="obsolete">Benutzerdefiniert</translation>
    </message>
    <message>
        <source>Dns</source>
        <translation type="obsolete">DNS</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1394"/>
        <source>Choose DNS service</source>
        <translation type="unfinished">Einen DNS server auswählen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1167"/>
        <source>Powersave Tweaks</source>
        <translation type="unfinished">Energiespar Tweaks</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1186"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatic CPU speed &amp;amp; power optimizer for Linux based on active monitoring of laptop&apos;s battery state, CPU usage, CPU temperature and system load. Ultimately allowing you to improve battery life without making any compromises.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1289"/>
        <source>Intel-undervolt enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1173"/>
        <source>Powersave tweaks</source>
        <translation type="unfinished">Energiespar Tweaks</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1260"/>
        <source>Thermald enabled</source>
        <translation type="unfinished">Thermald eingeschaltet</translation>
    </message>
    <message>
        <source>TLP enabled</source>
        <translation type="obsolete">TLP aktiviert</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1202"/>
        <source>Auto-cpufreq enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1407"/>
        <source>Switch Shells</source>
        <translation type="unfinished">Shell ändern</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1506"/>
        <source>Systemwide adblocking via /etc/hosts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1413"/>
        <source>Install default shell configs</source>
        <translation type="unfinished">Standardkonfiguration übernehmen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1083"/>
        <source>UFW enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1096"/>
        <source>Firewalld enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1461"/>
        <source>Default Shell:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1388"/>
        <source>DNS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1305"/>
        <source>GDM Wayland (GNOME only)</source>
        <translation type="unfinished">GDM Wayland (Nur GNOME)</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1311"/>
        <source>Enable GDM Wayland</source>
        <translation type="unfinished">Wayland in GDM einschalten</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="265"/>
        <source>BTRFS/Snapper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="335"/>
        <source>Launch BTRFS Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="389"/>
        <source>The BTRFS and Snapper functionality has been moved to a new application named BTRFS Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1273"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Intel CPU undervolting tool&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1215"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;TLP is a feature-rich command line utility for Linux, &lt;span style=&quot; font-weight:600;&quot;&gt;saving laptop battery power&lt;/span&gt; without the need to delve deeper into technical details.&lt;/p&gt;&lt;p&gt;TLP’s default settings are already &lt;span style=&quot; font-weight:600;&quot;&gt;optimized for battery life&lt;/span&gt; and implement Powertop’s recommendations out of the box. So you may just &lt;span style=&quot; font-weight:600;&quot;&gt;install and forget&lt;/span&gt; it&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1244"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The Linux Thermal Daemon program &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1372"/>
        <source>Preload enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1566"/>
        <source>Common settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1231"/>
        <source>Power-profiles-daemon enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1622"/>
        <source>System Specs</source>
        <translation type="unfinished">Systeminfos</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1658"/>
        <source>Refreshing.....</source>
        <translation type="unfinished">Neu laden.....</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1677"/>
        <location filename="../garudaassistant.ui" line="1774"/>
        <source>Copy for Forum</source>
        <translation type="unfinished">Für das Forum kopieren</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1696"/>
        <location filename="../garudaassistant.ui" line="1844"/>
        <source>Open Forum</source>
        <translation type="unfinished">Forum öffnen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1718"/>
        <source>Refresh</source>
        <translation type="unfinished">Neu laden</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1743"/>
        <source>Other Diagnostics</source>
        <translation type="unfinished">Andere Diagnosedaten</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1755"/>
        <source>Select a button below to load diagnostic information</source>
        <translation type="unfinished">Einen der unteren Buttons zum Daten laden drücken</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1812"/>
        <source>Systemd Analyze</source>
        <translation type="unfinished">Systemanalyse</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1793"/>
        <source>Journal Errors</source>
        <translation type="unfinished">Journal Fehlermeldung</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="158"/>
        <source>Garuda Assistant</source>
        <translation type="unfinished">Garuda Assistent</translation>
    </message>
    <message>
        <source>Elevated rights required for application function</source>
        <oldsource>Elevated rights required for application function

Exiting....</oldsource>
        <translation type="obsolete">Erweiterte Rechte zum Ausführen der Anwendung erforderlich</translation>
    </message>
    <message>
        <source>Exiting....</source>
        <translation type="obsolete">Anwendung wird beendet....</translation>
    </message>
    <message>
        <source>Authentication is required to run Garuda Assistant</source>
        <translation type="vanished">Erweiterte Rechte zum Ausführen der Anwendung erforderlich</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="267"/>
        <source>Failed to load groups for current user</source>
        <translation type="unfinished">Das Laden der Gruppen des Nutzers ist fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="409"/>
        <location filename="../garudaassistant.cpp" line="435"/>
        <source>Garuda Asssistant</source>
        <translation type="unfinished">Garuda Assistent</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="409"/>
        <source>Pacman database lock removed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="435"/>
        <source>Configs applied, please relogin to finish!</source>
        <translation type="unfinished">Konfiguration gespeichert, zum anwenden bitte neu anmelden!</translation>
    </message>
    <message>
        <source>Balance recommended.  Click here ----&gt;</source>
        <translation type="obsolete">Balancing empfohlen. Hier klicken ----&gt;</translation>
    </message>
    <message>
        <source>Balance not needed at this time</source>
        <translation type="obsolete">Balancing ist gerade nicht erforderlich</translation>
    </message>
    <message>
        <source>You have lots of free space, did you overbuy?</source>
        <translation type="obsolete">Du hast eine Menge freien Speicher, hast du zuviel gekauft?</translation>
    </message>
    <message>
        <source>Situation critical!  Time to delete some data or buy more disk</source>
        <translation type="obsolete">Situation kritisch! Zeit Daten zu löschen oder mehr Speicher zu kaufen</translation>
    </message>
    <message>
        <source>Your disk space is well utilized</source>
        <translation type="obsolete">Dein Datenspeicher ist gut genutzt</translation>
    </message>
    <message>
        <source>No device selected</source>
        <oldsource>No device selected
Please Select a device first</oldsource>
        <translation type="obsolete">Keine Gerät ausgewählt
Bitte erst ein Gerät auswählen</translation>
    </message>
    <message>
        <source>Please Select a device first</source>
        <translation type="obsolete">Bitte erst ein Gerät auswählen</translation>
    </message>
    <message>
        <source>Nothing to delete!</source>
        <translation type="obsolete">Nichts zum löschen!</translation>
    </message>
    <message>
        <source>Failed to delete subvolume!</source>
        <oldsource>Failed to delete subvolume!

subvolid mising from map</oldsource>
        <translation type="obsolete">Subvolume löschen fehlgeschlagen!</translation>
    </message>
    <message>
        <source>subvolid missing from map</source>
        <translation type="obsolete">Subvolid ist nicht auffindbar</translation>
    </message>
    <message>
        <source>You cannot delete a mounted subvolume</source>
        <oldsource>You cannot delete a mounted subvolume

Please unmount the subvolume before continuing</oldsource>
        <translation type="obsolete">Du kannst kein eingehängtes Subvolume löschen

Bitte vor dem fortfahren aushängen</translation>
    </message>
    <message>
        <source>Please unmount the subvolume before continuing</source>
        <translation type="obsolete">Bitte das Subvolume erst aushängen</translation>
    </message>
    <message>
        <source>Please Confirm</source>
        <translation type="obsolete">Bitte bestätigen</translation>
    </message>
    <message>
        <source>You are about to delete all subvolumes associated with timeshift snapshot </source>
        <translation type="obsolete">Wirklich alle Subvolumes löschen, die mit folgendem Snapshot assoziert sind </translation>
    </message>
    <message>
        <source>Are you sure you want to proceed?</source>
        <oldsource>

Are you sure you want to proceed?</oldsource>
        <translation type="obsolete">Bist du sicher, dass du fortfahren möchtest?</translation>
    </message>
    <message>
        <source>Snapshot Delete</source>
        <translation type="obsolete">Snapshot löschen</translation>
    </message>
    <message>
        <source>That subvolume is a snapper shapshot</source>
        <oldsource>That subvolume is a snapper shapshot

Please use the snapper tab to remove it</oldsource>
        <translation type="obsolete">Dieses Subvolume ist ein Snapper Snapshot

Bitte den Snapper Tab zum entfernen nutzen</translation>
    </message>
    <message>
        <source>Please use the snapper tab to remove it</source>
        <translation type="obsolete">Bitte den Snapper Tab zum löschen benutzen</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="obsolete">Bestätigen</translation>
    </message>
    <message>
        <source>Are you sure you want to delete </source>
        <translation type="obsolete">Bist du sicher, dass du dies löschen möchtest </translation>
    </message>
    <message>
        <source>Process failed with output:</source>
        <oldsource>Process failed with output:

</oldsource>
        <translation type="obsolete">Prozess mit folgender Meldung fehlgeschlagen:</translation>
    </message>
    <message>
        <source>Timeshift Snapshot</source>
        <translation type="obsolete">Timeshift Snapshot</translation>
    </message>
    <message>
        <source>Please Timeshift to restore this snapshot</source>
        <translation type="obsolete">Bitte Timeshift öffnen um diesen Snapshot wiederherzustellen</translation>
    </message>
    <message>
        <source>This is not a snapshot that can be restored by this application</source>
        <translation type="obsolete">Dies ist kein Snapshot welcher von dieser Anwendung wiederhergestellt werden kann</translation>
    </message>
    <message>
        <source>Failed to restore snapshot!</source>
        <oldsource>Failed to restore snapshot!

subvolid mising from map</oldsource>
        <translation type="obsolete">Wiederherstellung fehlgeschlagen!</translation>
    </message>
    <message>
        <source>Can&apos;t restore snapshot while the target subvolume is mounted</source>
        <translation type="obsolete">Kann keinen Snapshot wiederherstellen während das Ziel eingehängt ist</translation>
    </message>
    <message>
        <source>Target not found</source>
        <translation type="obsolete">Ziel nicht gefunden</translation>
    </message>
    <message>
        <source>Are you sure you want to restore </source>
        <translation type="obsolete">Sicher folgendes wiederherzustellen </translation>
    </message>
    <message>
        <source> to </source>
        <comment>as in from/to</comment>
        <translation type="obsolete"> nach </translation>
    </message>
    <message>
        <source>Failed to make a backup of target subvolume</source>
        <translation type="obsolete">Backup des Subvolumes fehlgeschlagen</translation>
    </message>
    <message>
        <source>Failed to restore subvolume!</source>
        <translation type="obsolete">Subvolume Wiederherstellung fehlgeschlagen!</translation>
    </message>
    <message>
        <source>Please verify the status of your system before rebooting</source>
        <translation type="obsolete">Bitte erst den Status des Systems vor den Neustart überprüfen</translation>
    </message>
    <message>
        <source>The restore was successful but the migration of the snapshots failed</source>
        <translation type="obsolete">Die Wiederherstellung war erfolgreich aber Migration der Snapshots ist fehlgeschlagen</translation>
    </message>
    <message>
        <source>Snapshot restoration complete.</source>
        <translation type="obsolete">Die Wiederherstellung ist abgeschlosssen.</translation>
    </message>
    <message>
        <source>A copy of the original subvolume has been saved as </source>
        <translation type="obsolete">Eine Kopie das ursprünglichen Subvolumes wurde wie folgt gesichert </translation>
    </message>
    <message>
        <source>Number</source>
        <comment>The number associated with a snapshot</comment>
        <translation type="obsolete">Nummer</translation>
    </message>
    <message>
        <source>This action cannot be undone</source>
        <translation type="obsolete">Diese Aktion kann nicht rückgängig gemacht werden</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="500"/>
        <source>hblock settings changed, please reboot to apply</source>
        <translation type="unfinished">Hblock Einstellungen geändert, bitte neustarten zum anwenden</translation>
    </message>
    <message>
        <source>Please migrate the .snapshots subvolume manually</source>
        <oldsource>The restore was successful but the migration of the snapshots failed

Please migrate the .snapshots subvolume manually</oldsource>
        <translation type="obsolete">Die Wiederherstellung war erfolgreich aber die Migration der Snapshots ist fehlgeschlagen

Bitte manuell fortfahren</translation>
    </message>
    <message>
        <source>Snapshot Restore</source>
        <translation type="obsolete">Snapshot Wiederherstellung</translation>
    </message>
    <message>
        <source>Please verify before rebooting</source>
        <oldsource>

Please verify before rebooting</oldsource>
        <translation type="obsolete">Bitte vor dem Neustart überprüfen</translation>
    </message>
    <message>
        <source>Date/Time</source>
        <translation type="obsolete">Datum/Zeit</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="obsolete">Beschreibung</translation>
    </message>
    <message>
        <source>No config selected for snapshot</source>
        <translation type="obsolete">Keine Konfiguration für Snapshot ausgewählt</translation>
    </message>
    <message>
        <source>Cannot delete snapshot</source>
        <translation type="obsolete">Dieser Snapshot kann nicht gelöscht werden</translation>
    </message>
    <message>
        <source>Failed to save changes</source>
        <translation type="obsolete">Änderungen speichern fehlgeschlagen</translation>
    </message>
    <message>
        <source>Changes saved</source>
        <translation type="obsolete">Änderungen übernommen</translation>
    </message>
    <message>
        <source>Please enter a valid name</source>
        <translation type="obsolete">Bitte einen gültigen Namen eingeben</translation>
    </message>
    <message>
        <source>That name is already in use!</source>
        <translation type="obsolete">Dieser Name existiert bereits!</translation>
    </message>
    <message>
        <source>No btrfs subvolumes found</source>
        <translation type="obsolete">Keine Subvolumes gefunden</translation>
    </message>
    <message>
        <source>No config selected</source>
        <translation type="obsolete">Keine Konfiguration ausgewählt</translation>
    </message>
    <message>
        <source>You may not don&apos;t delete the root config</source>
        <translation type="obsolete">Diese Konfiguration kann nicht gelöscht werden</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="30"/>
        <source>Authentication is required to perform this action.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="519"/>
        <source>Groups were added please logout so changes will take effect</source>
        <oldsource>Groups were added please logout so changes will take effect
</oldsource>
        <translation type="unfinished">Gruppen wurden hinzugefügt, zum anwenden bitte reloggen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="524"/>
        <source>Groups were removed please logout so changes will take effect</source>
        <oldsource>Groups were removed please logout so changes will take effect
</oldsource>
        <translation type="unfinished">Gruppen wurden entfernt, zum anwenden bitte reloggen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="528"/>
        <source>Service state updated</source>
        <translation type="unfinished">Service Status aktualisiert</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="544"/>
        <source>DNS Updated!</source>
        <translation type="unfinished">DNS geupdated!</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="575"/>
        <source>Shell changed, please relogin to apply changes!</source>
        <oldsource>Shell changed, please relogin to apply changes!
</oldsource>
        <translation type="unfinished">Shell verändert, zum anwenden bitte neu einloggen!</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="590"/>
        <source>Important!!</source>
        <translation type="unfinished">Wichtig!!</translation>
    </message>
    <message>
        <location filename="../garudaassistant.h" line="86"/>
        <source>All</source>
        <translation type="unfinished">Alles</translation>
    </message>
</context>
</TS>
