#ifndef GARUDAASSISTANT_H
#define GARUDAASSISTANT_H

#include <QClipboard>
#include <QDesktopServices>
#include <QDir>
#include <QFile>
#include <QMainWindow>
#include <QMap>
#include <QMessageBox>
#include <QProcess>
#include <QSet>
#include <QSignalMapper>
#include <QThread>
#include <QTime>
#include <QTranslator>
#include <QUuid>
#include <QXmlStreamReader>
#include <optional>

QT_BEGIN_NAMESPACE
namespace Ui {
class GarudaAssistant;
}
QT_END_NAMESPACE

class PermissionException {};

struct Result {
    int exitCode;
    QString output;
};

struct ConfigItem {
    QString source;
    QString command;
};

struct Terminal {
    QString binary;
    QString param;
};

struct Btrfs {
    QString mountPoint;
    long totalSize;
    long allocatedSize;
    long usedSize;
    long freeSize;
    long dataSize;
    long dataUsed;
    long metaSize;
    long metaUsed;
    long sysSize;
    long sysUsed;
    QMap<QString, QString> subVolumes;
};

struct SnapperSnapshots {
    int number;
    QString time;
    QString desc;
};

struct SnapperSubvolume {
    QString subvol;
    QString subvolid;
    QString time;
    QString desc;
    QString uuid;
};

class GarudaAssistant : public QMainWindow {
    Q_OBJECT

  protected:
    QSet<QString> unitsLoadedSet, unitsEnabledSet, globalUnitsLoadedSet, globalUnitsEnabledSet;
    QSet<QString> groupsEnabled;
    QHash<QString, QCheckBox *> configCheckBoxes;
    QProcess inxiProc, diagProc;
    QMap<QString, Btrfs> fsMap;
    QProcess bash;
    std::optional<QProcess> rootBash;

    const QMap<QString, ConfigItem> configActions = QMap<QString, ConfigItem>{
        {tr("All"), {"/etc/skel", "/usr/bin/cp -rf /etc/skel/. ~/."}},
        {"Latte", {"/etc/skel/.config/latte", "/usr/bin/cp -rf /etc/skel/.config/latte /etc/skel/.config/lattedockrc ~/.config/."}},
        {"Alacritty", {"/etc/skel/.config/alacritty", "/usr/bin/cp -rf /etc/skel/.config/alacritty ~/.config/."}},
        {"Fish", {"/etc/skel/.config/fish", "/usr/bin/cp -rf /etc/skel/.config/fish ~/.config/."}},
        {"Bleachbit", {"/etc/skel/.config/bleachbit", "/usr/bin/cp -rf /etc/skel/.config/bleachbit ~/.config/."}},
        {"Deluge", {"/etc/skel/.config/deluge", "/usr/bin/cp -rf /etc/skel/.config/deluge ~/.config/."}},
        {"Kvantum", {"/etc/skel/.config/Kvantum", "/usr/bin/cp -rf /etc/skel/.config/Kvantum ~/.config/."}},
        {"LibreOffice", {"/etc/skel/.config/libreoffice", "/usr/bin/cp -rf /etc/skel/.config/libreoffice ~/.config/."}},
        {"Libinput", {"/etc/skel/.config/libinput-gestures.conf", "/usr/bin/cp /etc/skel/.config/libinput-gestures.conf ~/.config/."}},
        {"Micro", {"/etc/skel/.config/micro", "/usr/bin/cp -rf /etc/skel/.config/micro ~/.config/."}},
        {"MPV", {"/etc/skel/.config/mpv", "/usr/bin/cp -rf /etc/skel/.config/mpv ~/.config/."}},
        {"Neofetch", {"/etc/skel/.config/neofetch", "/usr/bin/cp -rf /etc/skel/.config/neofetch ~/.config/."}},
        {"PSD", {"/etc/skel/.config/psd", "/usr/bin/cp -rf /etc/skel/.config/psd ~/.config/."}},
        {"Pulseaudio-ctl", {"/etc/skel/.config/pulseaudio-ctl", "/usr/bin/cp -rf /etc/skel/.config/pulseaudio-ctl ~/.config/."}},
        {"SMPlayer", {"/etc/skel/.config/smplayer", "/usr/bin/cp -rf /etc/skel/.config/smplayer ~/.config/."}},
        {"Strawberry", {"/etc/skel/.config/strawberry", "/usr/bin/cp -rf /etc/skel/.config/strawberry ~/.config/."}},
        {"Variety", {"/etc/skel/.config/variety", "/usr/bin/cp -rf /etc/skel/.config/variety ~/.config/."}},
        {"VLC", {"/etc/skel/.config/vlc", "/usr/bin/cp -rf /etc/skel/.config/vlc ~/.config/."}},
        {"Starship", {"/etc/skel/.config/starship.toml", "/usr/bin/cp /etc/skel/.config/starship.toml /etc/skel/.profile ~/."}},
        {"Bash", {"/etc/skel/.bashrc", "/usr/bin/cp /etc/skel/.bash* /etc/skel/.profile ~/."}},
        {"ZSH", {"/etc/skel/.zshrc", "/usr/bin/cp /etc/skel/.zshrc /etc/skel/.profile ~/."}}};

    QSet<QCheckBox *> changedCheckBoxes;
    const QMap<QString, QString> dnsChoices = QMap<QString, QString>{{"Adguard", "94.140.14.14"},
                                                                     {"Adguard Family Protection", "94.140.14.15"},
                                                                     {"Cloudflare", "1.1.1.1"},
                                                                     {"Cloudflare Malware and adult content blocking", "1.1.1.3"},
                                                                     {"DNS.Watch", "84.200.69.80"},
                                                                     {"OpenDNS", "208.67.220.220"},
                                                                     {"OpenDNS Familyshield", "208.67.220.123"},
                                                                     {"Quad9", "9.9.9.9"},
                                                                     {"Default", "0.0.0.0"}};
    QString getUserDefaultShell();

    bool checkAndInstall(QString package);
    void refreshInterface();
    void displayError(QString errorText);
    void loadEnabledUnits();
    void loadGlobalEnabledUnits();
    void loadGroups();
    bool isInstalled(QString packageName);
    void setupConfigBoxes();
    void apply();
    void setUnitState(QString service, bool enable, bool global = false);
    Result runCmd(QString cmd, bool includeStderr, bool escalate, int timeout = 60);
    Result runCmd(QStringList cmdList, bool includeStderr, bool escalate, int timeout = 60);
    int runCmdTerminal(QString cmd, bool escalate = false);
    int runCmdTerminal(QStringList cmdList, bool escalate = false);
    QString getCurrentDNS();
    bool checkGDMWayland();
    bool checkHblock();
    void refreshInxi();
    void startProcs();

  public:
    explicit GarudaAssistant(QWidget *parent = 0);
    ~GarudaAssistant();

    QString getVersion(QString name);

    QString version;
    QString output;

    bool setup();

  private slots:
    void inxiReturn();
    void diagnosticsReturn();
    void on_pushButton_reflector_clicked();
    void on_pushButton_sysup_clicked();
    void on_pushButton_orphans_clicked();
    void on_pushButton_clrcache_clicked();
    void on_pushButton_reinstall_clicked();
    void on_pushButton_dblck_clicked();
    void on_pushButton_editrepo_clicked();
    void on_pushButton_clrlogs_clicked();
    void on_pushButton_resetconfigs_clicked();
    void on_pushButton_applySettings_clicked();
    void on_pushButton_applySystem_clicked();
    void on_checkBox_clicked(QWidget *widget);
    void on_pushButton_refreshkeyring_clicked();
    void on_pushButton_refreshinxi_clicked();
    void on_pushButton_copyinxi_clicked();
    void on_pushButton_openforum_clicked();
    void on_pushButton_openforum_2_clicked();
    void on_pushButton_copydiag_clicked();
    void on_pushButton_analyze_clicked();
    void on_pushButton_pacman_logs_clicked();
    void on_pushButton_journal_clicked();
    void on_pushButton_launchAssistant_clicked();

  private:
    Ui::GarudaAssistant *ui;
};
#endif // GARUDAASSISTANT_H
