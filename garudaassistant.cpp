#include "garudaassistant.h"
#include "config.h"
#include "ui_garudaassistant.h"

#include <QTextStream>

GarudaAssistant::GarudaAssistant(QWidget *parent) : QMainWindow(parent), ui(new Ui::GarudaAssistant) { ui->setupUi(this); }

GarudaAssistant::~GarudaAssistant() {
    delete ui;
    if (rootBash) {
        runCmd("exit", false, true, 5000);
        rootBash->waitForFinished(5000);
    }
    runCmd("exit", false, false, 5000);
    bash.waitForFinished(5000);
}

// Util function for getting bash command output and error code
Result GarudaAssistant::runCmd(QString cmd, bool includeStderr, bool escalate, int timeout) {
    QByteArray testString = "gar7864373";
    QByteArray command = cmd.toUtf8();
    QProcess *proc;

    if (escalate) {
        if (!rootBash) {
            rootBash.emplace();
            rootBash->start("pkexec", QStringList() << SCRIPT_PATH "bg-rootshell.sh");
            if (runCmd("true", false, true, 0).exitCode) {
                displayError(tr("Authentication is required to perform this action."));
                rootBash->waitForFinished(5000);
                rootBash.reset();
                throw PermissionException();
            }
        }
        proc = &*rootBash;
    } else
        proc = &bash;

    if (includeStderr)
        proc->setProcessChannelMode(QProcess::MergedChannels);

    command += " ; echo " + testString + "\n";

    if (!proc->waitForStarted(1000))
        return {1, ""};

    proc->write(command);

    QString output = "";
    int startTime = time(NULL);
    do {
        if (proc->waitForReadyRead(timeout == 0 ? -1 : 1000 * timeout)) {
            output += proc->readAll();
        }
        if (output.right(testString.length() + 1).trimmed() == testString)
            break;

    } while ((time(NULL) - startTime < 60 || !timeout) && proc->state() == QProcess::ProcessState::Running);

    if ((time(NULL) - startTime >= 60 && timeout) || proc->state() == QProcess::ProcessState::NotRunning)
        return {1, output.trimmed()};

    output = output.left(output.length() - (testString.length() + 1));

    return {0, output.trimmed()};
}

// Util function for getting bash command output and error code
// This version takes a list so multiple commands can be executed at once
Result GarudaAssistant::runCmd(QStringList cmdList, bool includeStderr, bool escalate, int timeout) {
    QString fullCommand;
    for (auto command : cmdList) {
        if (fullCommand == "")
            fullCommand = command;
        else
            fullCommand += "; " + command;
    }

    // Run the composite command as a single command
    return runCmd(fullCommand, includeStderr, escalate, timeout);
}

// Runs a command in a terminal, escalate's using pkexec if escalate is true
int GarudaAssistant::runCmdTerminal(QString cmd, bool escalate) {
    QProcess proc;
    cmd += "; read -p 'Press enter to exit'";
    auto paramlist = QStringList();
    if (escalate)
        paramlist << "-s"
                  << "pkexec \"" SCRIPT_PATH "\"rootshell.sh";
    paramlist << cmd;
    proc.start("/usr/lib/garuda/launch-terminal", paramlist);
    proc.waitForFinished(-1);
    return proc.exitCode();
}

// Convenience function to run a list of commands at once
int GarudaAssistant::runCmdTerminal(QStringList cmdList, bool escalate) {
    QString fullCommand;
    for (auto command : cmdList) {
        if (fullCommand == "")
            fullCommand = command;
        else
            fullCommand += " ; " + command;
    }

    // Run the composite command as a single command
    return runCmdTerminal(fullCommand, escalate);
}

// Kickoff an async refresh of the inxiData
void GarudaAssistant::refreshInxi() {
    QFile::exists("/usr/bin/garuda-inxi") ? inxiProc.start("sh", QStringList() << "-c"
                                                                               << "garuda-inxi | sed 's/\x1b\[[0-9;]*[a-zA-Z]//g'")
                                          : inxiProc.start("/usr/bin/inxi", QStringList() << "-Fazc0");
}

// Process the inxi data and populate the QTextEdit
void GarudaAssistant::inxiReturn() {
    QString inxiOutput = inxiProc.readAllStandardOutput();

    const QStringList lines = inxiOutput.split("\n");
    for (const QString &line : lines) {
        if (!line.isEmpty()) {
            ui->textEdit_inxi->append(line);
        }
    }
}

// Process the diagnostics data and populate the QTextEdit
void GarudaAssistant::diagnosticsReturn() {
    const QString diag = diagProc.readAllStandardOutput();
    ui->textEdit_diagnostics->append(diag);
}

// Adds checkboxes for any items present in /etc/skel to config reset
void GarudaAssistant::setupConfigBoxes() {
    QMapIterator<QString, ConfigItem> i(configActions);
    int row = 0;
    int col = 0;

    while (i.hasNext()) {
        i.next();
        if (QFile::exists(i.value().source)) {
            configCheckBoxes[i.key()] = new QCheckBox(i.key());
            ui->gridLayout_config->addWidget(configCheckBoxes[i.key()], row, col);
            if (++col == 5) {
                row++;
                col = 0;
            }
        }
    }
}

// setup versious items first time program runs
bool GarudaAssistant::setup() {
    this->setWindowTitle(tr("Garuda Assistant"));

    // Check if we are booted off a snapshot
    bash.setProgram("/bin/bash");
    bash.start();

    // Show or hide UI elements
    if (!isInstalled("gdm")) {
        ui->groupBox_gdm->hide();
    }

    // Initialize combobox with list of shells
    ui->comboBox_shell->addItem("bash");
    ui->comboBox_shell->addItem("fish");
    ui->comboBox_shell->addItem("sh");
    ui->comboBox_shell->addItem("zsh");

    // Initialize the DNS combobox
    ui->comboBox_dns->addItem("");

    QMapIterator<QString, QString> i(dnsChoices);

    while (i.hasNext()) {
        i.next();
        ui->comboBox_dns->addItem(i.key());
    }

    // Create the config checkboxes
    setupConfigBoxes();

    refreshInxi();

    // connect the signal for the QProcesses
    connect(&diagProc, SIGNAL(readyReadStandardOutput()), this, SLOT(diagnosticsReturn()));
    connect(&inxiProc, SIGNAL(readyReadStandardOutput()), this, SLOT(inxiReturn()));

    // Connect all the checkboxes on the system components and settings tabs
    QSignalMapper *mapper = new QSignalMapper();
    connect(mapper, SIGNAL(mapped(QWidget *)), SLOT(on_checkBox_clicked(QWidget *)));

    auto checkboxList = ui->scrollArea_settings->findChildren<QCheckBox *>();
    checkboxList += ui->scrollArea_system->findChildren<QCheckBox *>();
    for (auto checkbox : checkboxList) {
        connect(checkbox, SIGNAL(clicked(bool)), mapper, SLOT(map()));
        mapper->setMapping(checkbox, checkbox);
    }

    refreshInterface();

    return true;
}

bool GarudaAssistant::checkAndInstall(QString package) {
    // If it's already installed, we are good to go
    if (isInstalled(package) || isInstalled(package + "-git"))
        return true;

    this->hide();
    runCmdTerminal("pacman -S " + package, true);

    this->show();
    return isInstalled(package);
}

void GarudaAssistant::displayError(QString errorText) { QMessageBox::critical(0, "Error", errorText); }

// Returns the current user's default shell
QString GarudaAssistant::getUserDefaultShell() {
    return runCmd("basename $(/usr/bin/getent passwd $USER | awk -F':' '{print $7}')", false, false).output;
}

// Populates servicesEnabledSet with a list of enabled services
void GarudaAssistant::loadEnabledUnits() {
    this->unitsLoadedSet.clear();
    this->unitsEnabledSet.clear();

    QString bashOutput = runCmd("systemctl list-unit-files -q --no-pager | tr -s \" \"", false, false).output;
    QStringList serviceList = bashOutput.split('\n');
    for (const QString &service : serviceList) {
        auto out = service.split(" ");
        this->unitsLoadedSet.insert(out[0]);
        if (out[1] == "enabled")
            this->unitsEnabledSet.insert(out[0]);
    }
}

// Populates Global servicesEnabledSet with a list of enabled services
void GarudaAssistant::loadGlobalEnabledUnits() {
    this->globalUnitsLoadedSet.clear();
    this->globalUnitsEnabledSet.clear();

    QString bashOutput = runCmd("systemctl --global list-unit-files -q --no-pager | tr -s \" \"", false, false).output;
    QStringList serviceList = bashOutput.split('\n');
    for (const QString &service : serviceList) {
        auto out = service.split(" ");
        this->globalUnitsLoadedSet.insert(out[0]);
        if (out[1] == "enabled")
            this->globalUnitsEnabledSet.insert(out[0]);
    }
}

// Get a list of groups for the current user
// We needs the groups that are set in /etc/group, not the currently active groups
void GarudaAssistant::loadGroups() {
    this->groupsEnabled.clear();

    // Load the data from /etc/group
    Result cmdResult = runCmd("getent group | grep $USER | awk -F: '{print $1\":\"$4}'", false, false);
    if (cmdResult.exitCode != 0) {
        displayError(tr("Failed to load groups for current user"));
        return;
    }

    // We will need the username to compare it to
    QString currentUser = qgetenv("USER");

    // Now we iterate over each line finding groups with exact matches
    QStringList groupEntries = cmdResult.output.split('\n');
    for (int i = 0; i < groupEntries.size(); ++i) {
        QStringList groupEntry = groupEntries.at(i).split(':');
        if (groupEntry.at(1) != "") {
            QStringList users = groupEntry.at(1).split(',');
            for (int j = 0; j < users.size(); ++j) {
                if (users.at(j) == currentUser)
                    this->groupsEnabled.insert(groupEntry.at(0));
            }
        }
    }
    return;
}

// If the service state is different than the desired state, enable or disable it
void GarudaAssistant::setUnitState(QString service, bool enable, bool global) {
    QString globalString;
    if (global)
        globalString += " --global";
    Result out;
    if (service != "" && unitsEnabledSet.contains(service) != enable) {
        if (enable)
            out = runCmd("systemctl enable" + globalString + " --now --force " + service, false, true);
        else
            out = runCmd("systemctl disable" + globalString + " --now " + service, false, true);
    }
}

// Returns true if packageName is installed and false if not
bool GarudaAssistant::isInstalled(QString packageName) { return runCmd("pacman -Qq " + packageName, false, false).output == packageName; }

// Returns the name of the dns service or an empty string if an unknown DNS server is in use
QString GarudaAssistant::getCurrentDNS() {
    QFile dnsFile("/etc/NetworkManager/conf.d/10-garuda-assistant-dns.conf");
    if (!dnsFile.open(QFile::ReadOnly | QFile::Text))
        return "Default";
    QTextStream dnsFileStream(&dnsFile);
    static QRegularExpression re("\\[global-dns-domain-\\*\\]\\nservers=([^,]*)\\n");
    QRegularExpressionMatch match = re.match(dnsFileStream.readAll());
    if (!match.hasMatch())
        return "Unknown";
    return dnsChoices.key(match.captured(1), "");
}

// If /etc/hosts contains more than 1 "Blocked Domains" line hblock must be enabled
bool GarudaAssistant::checkHblock() {
    QString hblock = runCmd("cat /etc/hosts | grep -A1 \"Blocked domains\" | awk '/Blocked domains/ { print $NF }'", false, false).output;
    return hblock.toInt() > 1;
}

bool GarudaAssistant::checkGDMWayland() {
    QString gdm = runCmd("cat /etc/gdm/custom.conf | grep -A1 WaylandEnable | awk '/WaylandEnable/ { print $NF }'", false, false).output;
    return !(gdm == "WaylandEnable=false");
}

// Updates the checkboxes and comboboxes with values from the system
void GarudaAssistant::refreshInterface() {
    loadEnabledUnits();
    loadGlobalEnabledUnits();
    loadGroups();

    // Loop through the checkboxes
    auto checkboxList = ui->scrollArea_settings->findChildren<QCheckBox *>();
    checkboxList += ui->scrollArea_system->findChildren<QCheckBox *>();
    for (auto checkbox : checkboxList) {
        if (checkbox->property("actionType") == "package") {
            checkbox->setChecked(isInstalled(checkbox->property("actionData").toString()));
        } else if (checkbox->property("actionType") == "global_service") {
            checkbox->setEnabled(this->globalUnitsLoadedSet.contains(checkbox->property("actionData").toString()));
            checkbox->setChecked(this->globalUnitsEnabledSet.contains(checkbox->property("actionData").toString()));
        } else if (checkbox->property("actionType") == "service") {
            checkbox->setEnabled(this->unitsLoadedSet.contains(checkbox->property("actionData").toString()));
            checkbox->setChecked(this->unitsEnabledSet.contains(checkbox->property("actionData").toString()));
        } else if (checkbox->property("actionType") == "group") {
            checkbox->setChecked(this->groupsEnabled.contains(checkbox->property("actionData").toString()));
        }
    }

    ui->comboBox_dns->setCurrentText(getCurrentDNS());

    ui->checkBox_hblock->setChecked(checkHblock());

    ui->checkBox_gdm->setChecked(checkGDMWayland());

    ui->comboBox_shell->setCurrentText(getUserDefaultShell());
}

/*######################################################################################
 *                                  Maintenance tab                                    *
######################################################################################*/

void GarudaAssistant::on_pushButton_reflector_clicked() {
    this->hide();
    runCmdTerminal("reflector-simple", false);
    this->show();
}

void GarudaAssistant::on_pushButton_refreshkeyring_clicked() {
    this->hide();
    // This command does like nothing to fix any actual issues tho, neither does it update the keyring??
    runCmdTerminal("pacman-key --init && pacman -Qs archlinux-keyring && pacman-key "
                   "--populate archlinux; pacman -Qs chaotic-keyring && pacman-key --populate chaotic; pacman -Qs "
                   "blackarch-keyring && pacman-key --populate blackarch",
                   true);
    this->show();
}

void GarudaAssistant::on_pushButton_sysup_clicked() {
    this->hide();
    runCmdTerminal("update");
    this->show();
}

void GarudaAssistant::on_pushButton_orphans_clicked() {
    this->hide();
    runCmdTerminal("pacman -Rns $(pacman -Qtdq)", true);
    this->show();
}

void GarudaAssistant::on_pushButton_clrcache_clicked() {
    this->hide();
    runCmdTerminal("paccache -ruk 0", true);
    this->show();
}

void GarudaAssistant::on_pushButton_reinstall_clicked() {
    this->hide();
    runCmdTerminal("pacman -S $(pacman -Qnq)", true);
    this->show();
}

void GarudaAssistant::on_pushButton_dblck_clicked() {
    try {
        if (!runCmd("rm /var/lib/pacman/db.lck", false, true).exitCode)
            QMessageBox::information(this, tr("Garuda Asssistant"), tr("Pacman database lock removed!"));
    } catch (PermissionException) {
        return;
    }
}

void GarudaAssistant::on_pushButton_editrepo_clicked() {
    this->hide();
    runCmd("pace", false, false);
    this->show();
}

void GarudaAssistant::on_pushButton_clrlogs_clicked() { runCmdTerminal(SCRIPT_PATH "clear-cache.sh", true); }

void GarudaAssistant::on_pushButton_resetconfigs_clicked() {
    bool isChanged = false;
    QHashIterator<QString, QCheckBox *> i(configCheckBoxes);
    while (i.hasNext()) {
        i.next();
        if (i.value()->isChecked()) {
            runCmd(configActions[i.key()].command, false, false);
            isChanged = true;
        }
    }

    if (isChanged)
        QMessageBox::information(0, tr("Garuda Asssistant"), tr("Configs applied, please relogin to finish!"));
}

/*######################################################################################
 *                           Settings tab                                              *
######################################################################################*/

// Apply any changes made on the System Components and Settings pages
void GarudaAssistant::apply() {
    // cmdList holds the list of commands so they can all be executed together and the user is only for their password once
    QStringList cmdList;
    QString groupsNew, groupsRemove, packagesInstall, packagesRemove, message;
    bool serviceChanged = false;

    try {
        // Start by going through the checkboxes which have changed
        for (auto checkbox : changedCheckBoxes) {
            QString actionData = checkbox->property("actionData").toString();
            QString actionType = checkbox->property("actionType").toString();

            // Now we go through each type taking an appropriate action for each
            if (actionType == "service") {
                if (actionData != "" && unitsLoadedSet.contains(actionData) &&
                    unitsEnabledSet.contains(actionData) != checkbox->isChecked()) {
                    setUnitState(actionData, checkbox->isChecked());
                    serviceChanged = true;
                }
            } else if (actionType == "global_service") {
                if (actionData != "" && globalUnitsLoadedSet.contains(actionData) &&
                    globalUnitsEnabledSet.contains(actionData) != checkbox->isChecked()) {
                    setUnitState(actionData, checkbox->isChecked(), true);
                    serviceChanged = false;
                }
            } else if (actionType == "group") {
                if (checkbox->isChecked() && !this->groupsEnabled.contains(actionData)) {
                    if (groupsNew != "")
                        groupsNew += ",";
                    groupsNew += actionData;
                } else if (!checkbox->isChecked() && this->groupsEnabled.contains(actionData)) {
                    if (groupsRemove != "")
                        groupsRemove += ",";
                    groupsRemove += actionData;
                    runCmd("gpasswd -d " + qgetenv("USER") + " " + actionData, false, true);
                }
            } else if (actionType == "package") {
                if (checkbox->isChecked()) {
                    if (packagesInstall != "")
                        packagesInstall += " ";
                    packagesInstall += actionData;
                    if (actionData == "pulseaudio-support") {
                        packagesInstall += " ";
                        packagesInstall += "pulseaudio-bluetooth";
                    }
                } else {
                    if (packagesRemove != "")
                        packagesRemove += " ";
                    packagesRemove += actionData;
                }
            } else if (actionType == "custom") {
                if (actionData == "hblock") {
                    if (checkbox->isChecked() != checkHblock()) {
                        if (checkbox->isChecked())
                            cmdList.append("/usr/bin/hblock");
                        else
                            cmdList.append("/usr/bin/hblock -S none -D none");
                        message += tr("hblock settings changed, please reboot to apply") + "\n";
                    }
                } else if (actionData == "gdm-wayland") {
                    if (checkbox->isChecked() != checkGDMWayland()) {
                        if (checkbox->isChecked())
                            runCmd(SCRIPT_PATH "gdmwayland-on.sh", false, true);
                        else
                            runCmd(SCRIPT_PATH "gdmwayland-off.sh", false, true);
                    }
                }
            }
        }

        // Empty the list of changed checkboxes since they have already been processed
        changedCheckBoxes.clear();

        // Add command for groups to be added
        if (!groupsNew.isEmpty()) {
            runCmd("usermod -aG " + groupsNew + " " + qgetenv("USER"), false, true);
            message += tr("Groups were added please logout so changes will take effect") + "\n";
        }

        // Add a message for groups to be removed
        if (!groupsRemove.isEmpty()) {
            message += tr("Groups were removed please logout so changes will take effect") + "\n";
        }

        if (serviceChanged)
            message += tr("Service state updated") + "\n";

        // Add command for packages to be installed
        if (!packagesInstall.isEmpty())
            cmdList.prepend("pacman -S " + packagesInstall + " --needed");

        // Add command for packages to be removed
        if (!packagesRemove.isEmpty())
            cmdList.prepend("pacman -Rs " + packagesRemove);

        // Check if the DNS servers need to be adjusted
        QString requestedDNS = ui->comboBox_dns->currentText();
        QString currentDNS = getCurrentDNS();

        if (requestedDNS != currentDNS && requestedDNS != "") {
            runCmd(SCRIPT_PATH "change-dns-server.sh " + dnsChoices.value(requestedDNS), false, true);
            message += tr("DNS Updated!") + "\n";
        }

        // See if the shell needs adjusting
        // We execute the shell commands seperately because we don't want them to be run with escalated privs
        QString requestedShell = ui->comboBox_shell->currentText();

        // Check if the selected shell is different from the user's default shell
        if (requestedShell != getUserDefaultShell()) {
            // check to see if we need to also copy the config
            bool copyConfig = ui->checkBox_shell->isChecked();

            if (requestedShell == "sh") {
                runCmd("usermod -s /bin/sh " + qgetenv("USER"), false, true);
            } else if (requestedShell == "bash") {
                runCmd("usermod -s /bin/bash " + qgetenv("USER"), false, true);
                if (copyConfig)
                    runCmd(configActions["bash"].command, false, false);
            } else if (requestedShell == "fish") {
                if (!checkAndInstall("garuda-fish-config"))
                    return;
                runCmd("usermod -s /bin/fish " + qgetenv("USER"), false, true);
                if (copyConfig)
                    runCmd(configActions["fish"].command, false, false);
            } else if (requestedShell == "zsh") {
                if (!checkAndInstall("garuda-zsh-config"))
                    return;
                runCmd("usermod -s /bin/zsh " + qgetenv("USER"), false, true);
                if (copyConfig)
                    runCmd(configActions["zsh"].command, false, false);
            }
            message += tr("Shell changed, please relogin to apply changes!") + "\n";
        }

        // Run all the commands at once in a terminal
        if (!cmdList.empty())
            runCmdTerminal(cmdList, true);

        // Reload the list of groups & services
        loadEnabledUnits();
        loadGlobalEnabledUnits();
        loadGroups();

        refreshInterface();

        if (!message.isEmpty())
            QMessageBox::warning(0, tr("Important!!"), message);
    } catch (PermissionException &exception) {
        // Bail out if we failed due to missing root
        return;
    }
}

// If a checkbox is clicked and that requires action, add it to a list to be processed by the apply button
void GarudaAssistant::on_checkBox_clicked(QWidget *widget) {
    QCheckBox *cb = static_cast<QCheckBox *>(widget);
    if (cb->property("actionType") != "none")
        changedCheckBoxes.insert(cb);
}

void GarudaAssistant::on_pushButton_applySettings_clicked() {
    apply();
    static_cast<QPushButton *>(QObject::sender())->clearFocus();
}

void GarudaAssistant::on_pushButton_applySystem_clicked() {
    apply();
    static_cast<QPushButton *>(QObject::sender())->clearFocus();
}

void GarudaAssistant::on_pushButton_refreshinxi_clicked() {
    ui->textEdit_inxi->clear();
    refreshInxi();
    ui->pushButton_refreshinxi->clearFocus();
}

void GarudaAssistant::on_pushButton_copyinxi_clicked() {
    QClipboard *clipboard = QGuiApplication::clipboard();

    clipboard->setText("```text\n" + ui->textEdit_inxi->toPlainText() + "\n```");
    ui->pushButton_copyinxi->clearFocus();
}

void GarudaAssistant::on_pushButton_openforum_clicked() {
    QDesktopServices::openUrl(QUrl("https://forum.garudalinux.org"));
    ui->pushButton_openforum->clearFocus();
}

void GarudaAssistant::on_pushButton_analyze_clicked() {
    ui->textEdit_diagnostics->clear();
    QString cmd = "/usr/bin/systemd-analyze --no-pager blame ; /usr/bin/systemd-analyze --no-pager critical-chain";
    diagProc.start("/usr/bin/bash", QStringList() << "-c" << cmd);

    ui->pushButton_analyze->clearFocus();
}

void GarudaAssistant::on_pushButton_pacman_logs_clicked() {
    ui->textEdit_diagnostics->clear();
    QString cmd = "tac /var/log/pacman.log | awk '!flag; /PACMAN.*pacman/{flag = 1};' | tac ";
    diagProc.start("/usr/bin/bash", QStringList() << "-c" << cmd);

    ui->pushButton_pacman_logs->clearFocus();
}

void GarudaAssistant::on_pushButton_journal_clicked() {
    ui->textEdit_diagnostics->clear();
    diagProc.start("/usr/bin/bash", QStringList() << "-c"
                                                  << "/usr/bin/journalctl -p err -b --no-pager");

    ui->pushButton_journal->clearFocus();
}

void GarudaAssistant::on_pushButton_copydiag_clicked() {
    QClipboard *clipboard = QGuiApplication::clipboard();

    clipboard->setText("```text\n" + ui->textEdit_diagnostics->toPlainText() + "\n```");
    ui->pushButton_copydiag->clearFocus();
}

void GarudaAssistant::on_pushButton_openforum_2_clicked() {
    QDesktopServices::openUrl(QUrl("https://forum.garudalinux.org"));
    ui->pushButton_openforum_2->clearFocus();
}

void GarudaAssistant::on_pushButton_launchAssistant_clicked() { QProcess::startDetached("sh", {"-c", "btrfs-assistant-launcher && true"}); }
